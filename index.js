'use strict';
const assert = require('assert');
const once = require('zd-once').once;

function waitFor() {
    var cb, dict, args;
    const start = Date.now();
    args = Array.prototype.slice.call(arguments);
    cb = args.pop();
    dict = args.reduce(function (acc, item) {
        acc[item] = false;
        return acc;
    }, {});
    assert(cb);
    assert.deepEqual(typeof cb, 'function');
    cb = once(cb);
    return function (service) {
        if (service instanceof Error) {
            return cb(service);
        }
        assert.deepEqual(typeof dict[service], 'boolean', 'unexpected service: ' + service);
        dict[service] = true;
        for (let service in dict) {
            if (!dict[service]) {
                return;
            }
        }
        return cb(null, Date.now() - start);
    };
}
exports = module.exports = waitFor;
