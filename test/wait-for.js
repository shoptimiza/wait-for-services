'use strict';
const assert = require('assert');
const waitFor = require('..');
describe('#waitFor(service, [services], callback)', function () {
    describe('Happy path', function () {
        before(function (done) {
            var self = this;
            const wf = waitFor('b', 'a', function (err, time) {
                if (err) {
                    return done(err);
                }
                self.took = time;
                done();
            });
            wf('a');
            setTimeout(function () {
                wf('b');
            }, 100);
        });
        it('should wait for services to get spawned', function () {
            var self = this;
            assert(self.took > 99);
        });
    });
    describe('When receiving an unexpected service to finish', function () {
        it('should throw an error', function () {
            const wf = waitFor('b', 'a', function () {
                assert.fail('Unexpected result');
            });
            try {
                wf('c');
            } catch (e) {
                assert.deepEqual(e.message, 'unexpected service: c');
                return;
            }
            assert.fail('Unexpected result');
        });
    });
    describe('When receiving an error instead of a service', function () {
        it('should callback that error', function (done) {
            const expectedError = new Error('expected error');
            const wf = waitFor('a', 'b', function (err) {
                assert.deepEqual(err, expectedError);
                return done();
            });
            wf(expectedError);
        });
    });
});
